import UIKit
import PlaygroundSupport
import Darwin

 // MARK: - Tasks
func task1() {
    print(1)
}

func task2() {
    print(2)
}

func task3() {
    sleep(1)
    print(3)
}

func task4() {
    print(4)
}

func task5() {
    print(5)
}

func task6() {
    sleep(1)
    print(6)
}

func task7() {
    print(7)
}

func task8() {
    print(8)
}


// MARK: Create serial Queue with Examples
//let serialQueue = DispatchQueue(label: "ru.romankhomenko.serial-queue")

// serial queue test (sync)
//task1()
//task2()
//serialQueue.sync(execute: task3) // this synchronize and main is waiting for finish this
//task4()
//task5()
//serialQueue.sync(execute: task6) // this synchronize and main is waiting for finish this
//task7()

// (task1)(task2)_______(task4)(task5)_______(task7)
//           sync|     |return    sync|     |return
//               |     |              |     |
//               (task3)              (task6)

// serial queue test (async)
//task1()
//task2()
//serialQueue.async(execute: task3) // main isnt waiting for finish this
//task4()
//task5()
//serialQueue.async(execute: task6) // main isnt waiting for finish this
//task7()

// (task1)(task2)(task4)(task5)(task7)
//               |            |
//          async|       async|
//               |            |
//               (task3)      (task6)

/*
// MARK: Task 1
let serialQueue = DispatchQueue(label: "ru.romankhomenko.serial-queue")

task1()
task2()
serialQueue.sync(execute: task3)
task4()
task5()
serialQueue.async(execute: task6)
task7()

// (task1)_______(task2)(task4)(task5)(task7)
//        |     |                    |
//   sync |     |return         async|
//        |     |                    |
//        (task3)                    (task6)
*/

/*
// MARK: Task 2
let serialQueue = DispatchQueue(label: "ru.romankhomenko.serial-queue")

task1()
task2()
serialQueue.async(execute: task3)
task4()
serialQueue.async(execute: task5)
task6()
serialQueue.sync(execute: task7)
task8()

// (task1) (task2) (task4) (task6)_____(task8)
//               |       |       |     |
//          async|  async|   sync|     |return
//               |       |       |     |
//               (task3) (task5) (task7)
*/

/*
// MARK: Task 3
let concurrentQueue = DispatchQueue(label: "ru.romankhomenko.concurrent-queue", attributes: .concurrent)

task1()
task2()
concurrentQueue.async(execute: task3)
task4()
concurrentQueue.async(execute: task5)
task6()
concurrentQueue.sync(execute: task7)
task8()

// (task1) (task2) (task4) (task6)_____(task8)
//               |       |       |     |
//          async|  async|   sync|     |return
//               |       |       |     |
//               (*** task3 ***) |     |
//                       |       |     |
//                       |       |     |
//                       (task5) |     |
//                               |     |
//                               (task7)
*/

/*
// MARK: Task 4
let concurrentQueue = DispatchQueue(label: "ru.romankhomenko.concurrent-queue", attributes: .concurrent)
let serialQueue = DispatchQueue(label: "ru.romankhomenko.serial-queue")

concurrentQueue.async {
    task1()
    
    serialQueue.sync(execute: task2)
}

concurrentQueue.sync {
    serialQueue.sync(execute: task3)
    
    serialQueue.sync(execute: task4)
    
    serialQueue.sync(execute: task5)
}

task6()

serialQueue.async {
    concurrentQueue.sync(execute: task7)
}

task8()
*/

/*
// MARK: ConcurrentQueue
// Создаем последовательную очередь
let serialQueue = DispatchQueue(label: "ru.denisegaluev.serial-queue")

// MARK: AsyncAfter
// Откладываем выполнение задачи в очереди serialQueue на 3 секунды и сразу же возвращаем управление
print("Right now")
serialQueue.asyncAfter(deadline: .now() + 3) {
    print("3 seconds later...")
}
*/


/*
// MARK: Semaphore

let concurrentQueue = DispatchQueue(label: "ru.denisegaluev.concurrent-queue", attributes: .concurrent)
let semaphore = DispatchSemaphore(value: 3)

for i in 0..<100 {
    concurrentQueue.async {
        print(i)
        
        sleep(1)
        
        // unblock semaphore
        semaphore.signal() // increment semaphore
    }

    semaphore.wait() // decrement semaphore
}
*/

/*
// MARK: DispathcGroup and WorkItems
let concurrentQueue = DispatchQueue(label: "ru.denisegaluev.concurrent-queue", attributes: .concurrent)

let group = DispatchGroup()

let workItem1 = DispatchWorkItem {
    print("workItem1: ZzzZZz")
    sleep(3)
    print("workItem1: awaked!")
    
    group.leave()
}

let workItem2 = DispatchWorkItem {
    print("workItem2: ZzzZZz")
    sleep(3)
    print("workItem2: awaked!")
    
    group.leave()
}

//concurrentQueue.async(group: group, execute: workItem1)
//concurrentQueue.async(group: group, execute: workItem2)
//
//group.notify(queue: DispatchQueue.main) {
//    print("All tasks on group completed")
//}

group.enter()
concurrentQueue.async(execute: workItem1)
group.enter()
concurrentQueue.async(execute: workItem2)

group.wait()
print("All tasks on group completed")
*/

// MARK: Dispatch barrier

/*
 
 Когда мы добавляем барьер в параллельную очередь, она откладывает выполнение задачи, помеченной барьером (и все остальные, которые поступят в очередь во время выполнения такой задачи), до тех пор, пока все предыдущие задачи не будут выполнены
 
*/
/*
class DispatchBarrierTesting {
    private let concurrentQueue = DispatchQueue(label: "ru.denisegaluev.concurrent-queue", attributes: .concurrent)
    
    private var _value: String = ""
    
    // Создаем thread safe переменную value для внешнего использования
    var value: String {
        get {
            var tmp: String = ""
            
            concurrentQueue.sync {
                tmp = _value
            }
            
            return tmp
        }
        
        set {
            // Данная реализация позволяет гарантировать, что в момент чтения, свойство value не будет изменено из другой очереди
            concurrentQueue.async(flags: .barrier) {
                self._value = newValue
            }
        }
    }
}
*/

// MARK: Problems

// MARK: Deadlock
// Deadlock — ситуация, в которой поток бесконечно ожидает доступ к ресурсу, который никогда не будет освобожден
/*
let serialQueue = DispatchQueue(label: "ru.denisegaluev.serial-queue")

print("Start")

// serialQueue никогда не вернет управление главной очереди, так как она будет бесконечно ожидать возврата управления от самой себя.
serialQueue.sync {
    serialQueue.sync {
        print("Deadlock")
    }
}

print("Finish")
*/

/*
// Fix DeadLock First example
let serialQueue = DispatchQueue(label: "ru.denisegaluev.serial-queue")

print("Start")

serialQueue.sync {
    serialQueue.async {
        print("We dont have Deadlock here")
    }
}

print("Finish")
*/


// Fix DeadLock Second example
let concurrentQueue = DispatchQueue(label: "ru.denisegaluev.concurrent-queue", attributes: .concurrent)

print("Start")

// В данном случае concurrentQueue не будет бесконечно ожидать возвращение управления, так как задачи будут выполняться на разных потоках.
concurrentQueue.sync {
    concurrentQueue.sync {
        print("We dont have Deadlock here")
    }
}

print("Finish")

// * Стоит избегать сложные вложенные планирования задач на одной последовательной очереди
// * Вызов sync у последовательно очереди из той же очереди всегда приведет к взаимной блокировке

// MARK: Race condition
// Race condition — ситуация, в которой ожидаемый порядок выполнения операций становится непредсказуемым, в результате чего страдает закладываемая логика
/*
var value: Int = 0
let serialQueue = DispatchQueue(label: "ru.denisegaluev.serial-queue")

func increment() { value += 1 }

serialQueue.async {
    sleep(5)
    increment() // this in the memory
}

print(value) // print before execution of increment() => value = 0

value = 10

serialQueue.sync {
    increment() // incremet()
}

// we expect value == 11, but in fact, we get 12
// because race condition
// we increment value = 10 two times => value == 12
print(value)
*/

/*
// Fix race condition
var value: Int = 0
let serialQueue = DispatchQueue(label: "ru.denisegaluev.serial-queue")

func increment() { value += 1 }

serialQueue.sync { // execute this synchronously
    sleep(5)
    increment()
}

print(value) // value == 11, because it wont print until sync blocke above is executed

value = 10 // value == 10

serialQueue.sync {
    increment() // incremet() value = 10
}

// we get value == 11
print(value)
*/
